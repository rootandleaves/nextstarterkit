/*
 * EXPORTS
 */
module.exports = {
  'siteUrl': 'https://rootandleaves.com',
  'changefreq': 'daily',
  'priority': 0.7,
  'sitemapSize': 5000,
  'generateRobotsTxt': true
}
