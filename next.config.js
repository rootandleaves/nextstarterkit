/*
 * IMPORTS
 */
const withPlugins = require('next-compose-plugins') // Npm: next.js component plugins utility.
const withTM = require('next-transpile-modules') // Npm: next.js module transpiler.
const withOptimizedImages = require('next-optimized-images') // Npm: next.js image optimizer.
const withFonts = require('next-fonts') // Npm: next.js fonts library.
const withPWA = require('next-pwa') // Npm: next.js pwa library
const withManifest = require('next-manifest') // Npm: next.js manifest.json Support.
const withImages = require('next-images') // Npm: next.js images support.
const { withSentryConfig } = require('@sentry/nextjs') // Npm: sentry error handling library.


/*
 * OBJECTS
 */
const _Plugins = withPlugins(
  [
    withImages,
    withFonts,
    [
      withTM,
      {
        'transpileModules': ['reusecore', 'common', 'lib']
      }
    ],
    [
      withOptimizedImages,
      {
        'mozjpeg': {
          'quality': 90
        },
        'webp': {
          'preset': 'default',
          'quality': 90
        }
      }
    ],
    [
      withPWA,
      {
        'pwa': {
          'dest': 'public',
          'disable': 'production' !== process.env.NODE_ENV
        }
      }
    ],
    [
      withManifest,
      {
        'manifest': {
          'output': './public/',
          'name': 'rootandleaves',
          'short_name': 'rootandleaves',
          'icons': [
            {
              'src': 'icons/icon-72x72.png',
              'size': '72x72',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-96x96.png',
              'size': '96x96',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-128x128.png',
              'size': '128x128',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-144x144.png',
              'size': '144x144',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-152x152.png',
              'size': '152x152',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-192x192.png',
              'size': '192x192',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-384x384.png',
              'size': '384x384',
              'type': 'image/png'
            },
            {
              'src': 'icons/icon-512x512.png',
              'size': '512x512',
              'type': 'image/png'
            }
          ],
          'start_url': '.',
          'display': 'minimal-ui',
          'orientation': 'portrait',
          'background_color': '#0f2137',
          'theme_color': '#4caf50'
        }
      }
    ],
    {
      webpack(config, { webpack }) {
        /*
         * Note: we provide webpack above so you should not `require` it
         * Perform customizations to webpack config
         */
        config.module.rules.push({
          'test': /\.(glsl|frag|vert)$/u,
          'use': ['glslify-import-loader', 'raw-loader', 'glslify-loader']
        })
        config.plugins.push(new webpack.ProvidePlugin({ 'THREE': 'three' }))
        config.resolve.fallback = { 'fs': false }

        // Important: return the modified config
        return config
      }
    }
  ],
  {
    'target': 'production' !== process.env.NODE_ENV || !process.env.NODE_ENV ? 'server' : 'serverless',
    'cleanDistDir': true,
    'generateEtags': true,
    'reactStrictMode': true,
    'webpack5': true,
    'optimizeFonts': true,
    'optimizeImages': true,
    'trailingSlash': true,
    'eslint': {
      'ignoreDuringBuilds': true
    },
    'experimental': {
      'esmExternals': true,
      'staticPageGenerationTimeout': 120
    },
    'devIndicators': {
      'buildActivity': false
    },
    'images': {
      'domains': ['localhost', 'rootandleaves.com', 'www.awesomescreenshot.com', 'awesomescreenshot.s3.amazonaws.com']
    }
  }
)
const _configuration = { 'silent': true }


/*
 * EXPORTS
 */
module.exports = withSentryConfig(_Plugins, _configuration)
