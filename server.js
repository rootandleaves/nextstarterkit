/*
 * IMPORTS
 */
const Express = require('express') // Npm: express server library.
const Next = require('next') // Npm: next.js library.
const { join } = require('path') // Npm: node.js path library.
const { parse } = require('url') // Npm: node.js url library.


/*
 * GLOBALS
 */
const dev = 'production' !== process.env.NODE_ENV


/*
 * OBJECTS
 */
const App = Next({ dev })
const Handle = App.getRequestHandler()


/*
 * SERVER
 */
App.prepare().then(async () => {
  // Error handling.
  try {
    const server = Express()

    // Server route.
    server.get('*', (__request, __response) => {
      const parsedUrl = parse(__request.url, true)
      const rootStaticFiles = [
        '/manifest.json',
        '/sitemap.xml',
        '/favicon.ico',
        '/robots.txt',
        '/sw.js',
        '/browserconfig.xml',
        '/site.webmanifest'
      ]
      const { pathname } = parsedUrl

      if (-1 < rootStaticFiles.indexOf(pathname) || pathname.startsWith('/workbox-')) {
        // Return static files
        return App.serveStatic(__request, __response, join(__dirname, 'public', pathname))
      }

      // Return response as is.
      return Handle(__request, __response, parsedUrl)
    })

    // Server listener.
    return server.listen(process.env.PORT || 4000, error => error instanceof Error ? error : void 0)
  } catch (error) {
    // Report failure.
    return error
  }
})
