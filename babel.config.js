/*
 * EXPORTS
 */
module.exports = __api => {
  // Use cache.
  __api.cache(true)

  // Babel presets and plugins.
  const presets = ['next/babel', '@babel/preset-env']
  const plugins = [
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime',
    'babel-plugin-root-import',
    'starts-with-ends-with',
    [
      'styled-components',
      {
        'ssr': false,
        'displayName': true,
        'preprocess': true
      }
    ]
  ]

  /*
   * Return presets and plugins
   * with project root defined.
   */
  return {
    presets,
    plugins,
    'babelrcRoots': [
      '.',
      './*',
      'packages/*'
    ]
  }
}
