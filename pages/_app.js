/*
 * IMPORTS
 */
import Head from 'next/head' // Npm: babel polyfill library.
import React from 'react' // Npm: react library.
import NextNprogress from 'nextjs-progressbar' // Npm: next.js progress bar.
import { Provider } from 'react-redux' // Npm: react redux library.
import { PersistGate } from 'redux-persist/integration/react' // Npm: react persist library.
import { ThemeProvider } from 'styled-components' // Npm: styled-components for react.js
import { gsap } from 'gsap/dist/gsap' // Npm: gsap library.
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger' // Npm: scroll trigger library.
import { ScrollToPlugin } from 'gsap/dist/ScrollToPlugin' // Npm: scroll to plugin library.


/*
 * PACKAGES
 */
import '~packages/iife'
import Redux, { Persist } from '~packages/redux'


/*
 * STYLES
 */
import { AppWrapper, GlobalStyle } from '~packages/common/style'


/*
 * INITIALIZATION
 */
gsap.registerPlugin(ScrollTrigger)
gsap.registerPlugin(ScrollToPlugin)


/*
 * OBJECTS
 */
const App = ({ Component, pageProps }) => {
  // Variable assignment.
  const { Theme } = Redux.getState()

  // Hook assignment.
  const [theme] = React.useState(Theme)

  // Return component.
  return (
    <Provider store={Redux}>
      <PersistGate persistor={Persist}>
        <ThemeProvider theme={theme}>
          <Head>
            <title>App</title>
            <meta name='application-name' content='rootandleaves.com' />
            <meta name='theme-color' content='#0f0c29'/>
            <meta name='apple-mobile-web-app-capable' content='yes' />
            <meta name='apple-mobile-web-app-status-bar-style' content='default' />
            <meta name='apple-mobile-web-app-title' content='rootandleaves.com' />
            <meta name='description' content='webservices platform' />
            <meta name='format-detection' content='telephone=no' />
            <meta name='mobile-web-app-capable' content='yes' />
            <meta name='msapplication-config' content='/static/browserconfig.xml' />
            <meta name='msapplication-TileColor' content='#0f0c29' />
            <meta name='msapplication-tap-highlight' content='no' />
            <meta name='theme-color' content='#0f0c29' />
            <meta name='viewport' content='minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, maximum-scale=5, viewport-fit=cover' />
            <meta property='og:type' content='website' />
            <meta property='og:title' content='rootandleaves.com' />
            <meta property='og:description' content='Website development and it services.' />
            <meta property='og:site_name' content='rootandleaves.com' />
            <meta property='og:url' content='https://rootandleaves.com' />
            <meta property='og:image' content='/static/image/apple-touch-icon.png' />
            <link rel='icon' type='image/svg+xml' href='/static/image/favicon.svg' />
            <link rel='icon' type='image/png' href='/static/image/favicon.png' />
            <link rel='apple-touch-icon' sizes='180x180' href='/static/image/apple-touch-icon.png' />
            <link rel='icon' type='image/png' sizes='32x32' href='/static/image/favicon-32x32.png' />
            <link rel='icon' type='image/png' sizes='16x16' href='/static/image/favicon-16x16.png' />
            <link rel='manifest' href='/static/site.webmanifest' />
            <link rel='mask-icon' href='/static/image/safari-pinned-tab.svg' color='#2948ff' />
            <meta name='msapplication-TileColor' content='#2b5797' />
            <link rel='preconnect' href='https://fonts.gstatic.com' />
            <link href='https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' media='print' onLoad='this.media="all"' />
            <meta name='viewport' content='width=device-width, initial-scale=1' />
            <script
              async
              src='https://www.googletagmanager.com/gtag/js?id=UA-171842846-1'
            />
            <script
              dangerouslySetInnerHTML={{
                '__html': `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'UA-171842846-1', { page_path: window.location.pathname });
            `
              }}
            />
          </Head>
          <GlobalStyle />
          <NextNprogress
            color={theme.colors.primary}
            startPosition={0.3}
            stopDelayMs={200}
            height={3}
            showOnShallow={true}
            options={{ 'showSpinner': false }}
          />
          <AppWrapper>
            <Component {...pageProps}/>
          </AppWrapper>
        </ThemeProvider>
      </PersistGate>
    </Provider>
  )
}


/*
 * EXPORTS
 */
export default App
