/*
 * IMPORTS
 */
import React from 'react' // Npm: react.js library.
import Head from 'next/head' // Npm: next.js header utility.


/*
 * OBJECTS
 */
const Index = () => {
  // Return component.
  return (
    // Return component.
    <section>
      <Head>
        <title>Title goes here</title>
        <meta name='Description' content='Home | Rootandleaves Webservices'/>
      </Head>
      <main>
        <h1>Hello</h1>
      </main>
    </section>
  )
}


/*
 * PROPTYPES
 */
Index.defaultProps = {}


/*
 * EXPORTS
 */
export default Index
