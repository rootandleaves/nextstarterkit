/*
 * IMPORTS
 */
import React from 'react' // Npm: react.js library.
import Head from 'next/head' // Npm: next.js library head component.


/*
 * PACKAGES
 */
import ErrorComponent from '~packages/common/components/Error'


/*
 * EXPORTS
 */
export default class Error extends React.Component {
  /*
   * Render object for rendering
   * components.
   */
  render() {
    // Return component.
    return (
      <section>
        <Head>
          <title>404: Not found</title>
          {this.props.styleTags}
        </Head>
        {this.props.statusCode ? (
          `An error ${this.props.statusCode} occurred on server`
        ) : (
          <ErrorComponent />
        )}
      </section>
    )
  }
}
