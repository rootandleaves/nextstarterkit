/*
 * IMPORTS
 */
import React from 'react' // Npm: react.js library.
import Head from 'next/head' // Npm: next.js head library.
import Document, { Html, Main, NextScript } from 'next/document' // Npm: next.js library.
import Manifest from 'next-manifest/manifest' // Npm: next.js manifest handler.
import { ServerStyleSheet } from 'styled-components' // Npm: styled component library.


/*
 * OBJECTS
 */
class MyDocument extends Document {
  // GetInitialProps for getting initial props.
  static async getInitialProps(__ctx) {
    // Const assignment.
    const _sheet = new ServerStyleSheet()
    const _originalRenderPage = __ctx.renderPage
    const _initialProps = await Document.getInitialProps(__ctx)

    // Load all styles from application.
    __ctx.renderPage = () =>
      _originalRenderPage({
        'enhanceApp': App => props =>
          _sheet.collectStyles(<App {...props} />)
      })

    // Return component.
    return {
      ..._initialProps,
      'styles': (
        <Html>
          <Head>
            <Manifest href='static/manifest.json'/>
            <title />
          </Head>
          {_initialProps.styles}
          {_sheet.getStyleElement()}
          <Main />
          <NextScript />
        </Html>
      )
    }
  }
}


/*
 * EXPORTS
 */
export default MyDocument
