/*
 * IMPORTS
 */
import __Debug from 'debug' // Npm: debug utility module.
import {
  AmbientLight,
  AnimationMixer,
  Box3,
  Cache,
  DirectionalLight,
  HemisphereLight,
  LinearEncoding,
  LoaderUtils,
  LoadingManager,
  PerspectiveCamera,
  PMREMGenerator,
  REVISION,
  Scene,
  sRGBEncoding,
  UnsignedByteType,
  Vector3,
  WebGLRenderer
} from 'three' // Npm: three.js gltf loader.
import Stats from 'three/examples/jsm/libs/stats.module.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js' // Npm: three.js GLTF loader.
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader.js' // Npm: three.js KTX2 loader.
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js' // Npm: three.js DRACO loader.
import { MeshoptDecoder } from 'three/examples/jsm/libs/meshopt_decoder.module.js' // Npm: three.js MeshOpt loader.
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js' // Npm: three.js Orbit Controller loader.
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js' // Npm: three.js RGBE loader.


/*
 * GLOBALS
 */
const _defaultCamera = '[default]'
const _maps = [
  'map',
  'aoMap',
  'emissiveMap',
  'glossinessMap',
  'metalnessMap',
  'normalMap',
  'roughnessMap',
  'specularMap'
]
const _preset = { 'ASSET_GENERATOR': 'assetgenerator' }
const _defaultOptions = { 'closeGUI': false, 'cameraPosition': void 0, 'instanceLoadable': false, 'extension': 'glb' }
const _Debug = __Debug('GLTFLoader')


/*
 * CONFIG
 */
Cache.enabled = true


/*
 * OBJECTS
 */
const _LoadingManager_ = new LoadingManager()
const _DracoManager_ = new DRACOLoader(_LoadingManager_).setDecoderPath(`https://unpkg.com/three@0.${REVISION}.x/examples/js/libs/draco/gltf/`)
const _KtxManager_ = new KTX2Loader(_LoadingManager_).setTranscoderPath(`https://unpkg.com/three@0.${REVISION}.x/examples/js/libs/basis/`)


/*
 * CLASS
 */
class Viewer {
  // Constructor of given class.
  constructor(__el, $options) {
    // Variable assignment.
    $options = { ..._defaultOptions, ...$options }

    // Const assignment.
    const _fov = $options.preset === _preset.ASSET_GENERATOR ? 0.8 * 180 / Math.PI : 60

    // Variable assignment.
    window.renderer = new WebGLRenderer({ 'antialias': true, 'alpha': true })

    // Properties.
    this.el = __el
    this.options = $options
    this.lights = []
    this.content = void 0
    this.mixer = void 0
    this.clips = []
    this.state = {
      'playbackSpeed': 0.001,
      'actionStates': {},
      'camera': _defaultCamera,
      'addLights': true,
      'exposure': 0.70,
      'textureEncoding': 'sRGB',
      'ambientIntensity': 0.71,
      'ambientColor': 0xFFFFFF,
      'directIntensity': 0.7 * Math.PI,
      'directColor': 0xFFFFFF,
      'bgColor1': '#ffffff',
      'bgColor2': '#353535'
    }
    this.prevTime = 0
    this.stats = new Stats()
    this.stats.dom.height = '48px'
    this.scene = new Scene()
    this.defaultCamera = new PerspectiveCamera(_fov, __el.clientWidth / __el.clientHeight, 0.01, 1000)
    this.activeCamera = this.defaultCamera
    this.scene.add(this.defaultCamera)
    this.renderer = window.renderer
    this.renderer.physicallyCorrectLights = true
    this.renderer.outputEncoding = sRGBEncoding
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.setSize(__el.clientWidth, __el.clientHeight)
    this.pmremGenerator = new PMREMGenerator(this.renderer)
    this.pmremGenerator.compileEquirectangularShader()
    this.controls = new OrbitControls(this.defaultCamera, this.renderer.domElement)
    this.controls.autoRotate = Boolean($options.autoRotate)
    this.controls.autoRotateSpeed = -10
    this.controls.screenSpacePanning = true

    // Update orbital controls options.
    this.controls.enableDamping = true
    this.controls.enableZoom = false
    this.controls.enableRotate = true
    this.controls.enablePan = false

    // Update scene to dom.
    this.el.appendChild(this.renderer.domElement)
    this.animate = this.animate.bind(this)

    // Event listener.
    requestAnimationFrame(this.animate)

    // Event listener.
    window.addEventListener('resize', this.resize.bind(this), false)
  }

  /*
   * Static method for handling mesh
   * materials by calling callback.
   */
  static traverseMaterials(__object, __callback) {
    // Traverse object node and run call back.
    __object.traverse(__node => {
      /*
       * If given node is not mesh than
       * return as is.
       */
      if (!__node.isMesh) return

      // Const assignment.
      const materials = Array.isArray(__node.material) ? __node.material : [__node.material]

      // Run call back for given material.
      materials.forEach(__callback)
    })
  }

  /*
   * Animation handler for handling
   * animation frames of renderer.
   */
  animate(__time) {
    // Update animation frame.
    requestAnimationFrame(this.animate)

    // Update properties.
    this.controls.update()
    this.stats.update()
    this.mixer && this.mixer.update((__time - this.prevTime) / 1000)
    this.render()

    // Update time.
    this.prevTime = __time
  }

  /*
   * Render handler for rendering all
   * scenes and camera.
   */
  render() {
    // Render scene and camera.
    this.renderer.render(this.scene, this.activeCamera)
  }

  /*
   * Resize handler for handling element
   * view width and height property so that
   * scene could be re-rendered accordingly.
   */
  resize() {
    // Const assignment.
    const { clientHeight, clientWidth } = this.el.parentElement

    // Update properties.
    this.defaultCamera.aspect = clientWidth / clientHeight
    this.defaultCamera.updateProjectionMatrix()
    this.renderer.setSize(clientWidth, clientHeight)
  }

  /*
   * Gltf or Glb file loader for loading
   * all respective scenes and properties.
   */
  load(__url, __rootPath, __assetMap) {
    // Const assignment.
    const _blobURLs = []
    const _baseURL = LoaderUtils.extractUrlBase(__url)

    // Return promise for loading given file.
    return new Promise((__Resolve, __Reject) => {
      // Intercept and override relative URLs.
      _LoadingManager_.setURLModifier((__uri, path) => {
        // Const assignment.
        const _normalizedURL = __rootPath + decodeURI(__uri).replace(_baseURL, '').replace(/^(\.?\/)/u, '')

        // Load asset map if available.
        if (__assetMap && __assetMap.has(_normalizedURL)) {
          // Const assignment.
          _blobURLs.push(URL.createObjectURL(__assetMap.get(_normalizedURL)))

          // Return last inserted object.
          return _blobURLs[_blobURLs.length - 1]
        }

        // Return normalized uri.
        return (path || '') + __uri
      })

      // Create loader instance.
      const _Loader_ = new GLTFLoader(_LoadingManager_).setCrossOrigin('anonymous').setDRACOLoader(_DracoManager_).setKTX2Loader(_KtxManager_.detectSupport(this.renderer)).setMeshoptDecoder(MeshoptDecoder)

      // Load file at given url.
      _Loader_.load(__url, __gltf => {
        // Const assignment.
        const _scene = __gltf.scene || __gltf.scenes[0]
        const _clips = __gltf.animations || []

        // Valid, but not supported by this viewer.
        if (!_scene) throw new Error('This model contains no scene, and cannot be viewed here. However, it may contain individual 3D resources.')

        // Update scene and clips.
        this.setContent(_scene, _clips)

        /*
         * Only center to y-axis if given
         * el is banner.
         */
        if (this.options.isBanner) __gltf.scene.rotation.y = -11

        /*
         * Loop over each url and get
         * uri by revoking given url.
         */
        _blobURLs.forEach(URL.revokeObjectURL)

        // Resolve given __gltf.
        __Resolve(__gltf)
      }, void 0, __Reject)
    })
  }

  /*
   * Update object content and clips of
   * animation to scene.
   */
  setContent(__object, __clips) {
    // Clear scene before continuing.
    this.clear()

    // Const assignment.
    const _box = new Box3().setFromObject(__object)
    const _size = _box.getSize(new Vector3()).length()
    const _center = _box.getCenter(new Vector3())

    // Reset control of scene.
    this.controls.reset()

    /*
     * Update object coordinates
     * in scene.
     */
    __object.position.x += (__object.position.x - _center.x)
    __object.position.y += (__object.position.y - _center.y)
    __object.position.z += (__object.position.z - _center.z)

    // Update properties.
    this.controls.maxDistance = _size * 10
    this.defaultCamera.near = _size / 100
    this.defaultCamera.far = _size * 100
    this.defaultCamera.updateProjectionMatrix()

    /*
     * If camera position is defined on
     * this context use it else center it.
     */
    if (this.options.cameraPosition) {
      // Update properties.
      this.defaultCamera.position.fromArray(this.options.cameraPosition)
      this.defaultCamera.lookAt(new Vector3())
    } else {
      // Update properties.
      this.defaultCamera.position.copy(_center)
      this.defaultCamera.position.x += _size / (this.options.isBanner ? 2.2 : 1.9)
      this.defaultCamera.position.y -= _size / (this.options.isBanner ? -26.5 : 2.0)
      this.defaultCamera.position.z += _size / (this.options.isBanner ? 91.5 : 1.0)
      this.defaultCamera.lookAt(_center)
    }

    // Update camera to scene.
    this.setCamera(_defaultCamera)

    // Save given controls.
    this.controls.saveState()

    // Update scene.
    this.scene.add(__object)

    // Update properties.
    this.content = __object
    this.state.addLights = true
    this.content.traverse(__node => {
      // Check if given node is light.
      if (__node.isLight) {
        /*
         * Than update state lights to
         * false.
         */
        this.state.addLights = false
      } else if (__node.isMesh) {
        // Else add transparent material on depth write.
        __node.material.depthWrite = !__node.material.transparent
      }
    })
    this.setClips(__clips)
    this.updateLights()
    this.updateTextureEncoding()

    // Update window content.
    window.content = this.content

    // Debug message.
    _Debug('[glTF Viewer] THREE.Scene exported as `window.content`.')
  }

  /*
   * Set animation clips if any available.
   * with animation mixer.
   */
  setClips(__clips) {
    // If mixer is available.
    if (this.mixer) {
      /*
       * Remove all previous junk
       * even from cache and reset mixer.
       */
      this.mixer.stopAllAction()
      this.mixer.uncacheRoot(this.mixer.getRoot())
      this.mixer = void 0
    }

    // Update given clips.
    this.clips = __clips

    // If clip is empty then return as is.
    if (!__clips.length) return

    // Update animation.
    this.mixer = new AnimationMixer(this.content)
  }

  /*
   * Play animation available on
   * mixer and update states too.
   */
  playAllClips() {
    /*
     * Loop over all available clips
     * and update their state.
     */
    this.clips.forEach(__clip => {
      /*
       * Play animation and update given
       * clip property.
       */
      this.mixer.clipAction(__clip).reset().play()
      this.state.actionStates[__clip.name] = true
    })
  }


  /*
   * Update camera of given scene.
   * and properties.
   */
  setCamera(__name) {
    /*
     * If given camera is default camera
     * than update control and mark default
     * camera as active on.
     */
    if (__name === _defaultCamera) {
      // Update properties.
      this.controls.enabled = true
      this.activeCamera = this.defaultCamera
    } else {
      // Set controls to false.
      this.controls.enabled = false

      /*
       * Traverse over content and update each
       * camera on node.
       */
      this.content.traverse(__node => {
        // Mark given node camera as active.
        if (__node.isCamera && __node.name === __name) this.activeCamera = __node
      })
    }
  }

  /*
   * Update textures and encoding
   * of given scene with rgb.
   */
  updateTextureEncoding() {
    // Const assignment.
    const _encoding = 'sRGB' === this.state.textureEncoding ? sRGBEncoding : LinearEncoding

    /*
     * Traverse materials of given content
     * set.
     */
    Viewer.traverseMaterials(this.content, material => {
      /*
       * Conditional update of material
       * properties.
       */
      if (material.map) material.map.encoding = _encoding
      if (material.emissiveMap) material.emissiveMap.encoding = _encoding
      if (material.map || material.emissiveMap) material.needsUpdate = true
    })
  }


  /*
   * Update lights of scene on behalf
   * of state available.
   */
  updateLights() {
    // Const assignment.
    const _state = this.state
    const _lights = this.lights

    // Add light if state is available.
    if (_state.addLights && !_lights.length) {
      // Add light to scene.
      this.addLights()
    } else if (!_state.addLights && _lights.length) {
      // Remove light from scene.
      this.removeLights()
    }

    // Update tone exposure.
    this.renderer.toneMappingExposure = _state.exposure

    // Update lights properties.
    if (2 === _lights.length) {
      // Update properties.
      _lights[0].intensity = _state.ambientIntensity
      _lights[0].color.setHex(_state.ambientColor)
      _lights[1].intensity = _state.directIntensity
      _lights[1].color.setHex(_state.directColor)
    }
  }

  /*
   * Add lights too scene
   * with given state.
   */
  addLights() {
    // Const assignment.
    const state = this.state

    // If assets provided matches to preset.
    if (this.options.preset === _preset.ASSET_GENERATOR) {
      // Const assignment.
      const _HemisphereLight_ = new HemisphereLight()

      // Update properties.
      _HemisphereLight_.name = 'hemi_light'

      /*
       * Update scene with light and store
       * given light.
       */
      this.scene.add(_HemisphereLight_)
      this.lights.push(_HemisphereLight_)

      // Return void.
      return void 0
    }

    // Add ambient light to scene.
    const _AmbientLight_ = new AmbientLight(state.ambientColor, state.ambientIntensity)

    // Update properties.
    _AmbientLight_.name = 'ambient_light'

    // Add directional light to scene.
    const _DirectionalLight_ = new DirectionalLight(state.directColor, state.directIntensity)

    // Update properties.
    _DirectionalLight_.position.set(0.5, 0, 0.866)
    _DirectionalLight_.name = 'main_light'

    // Add lights to camera.
    this.defaultCamera.add(_AmbientLight_)
    this.defaultCamera.add(_DirectionalLight_)

    // Store newly created light.
    this.lights.push(_AmbientLight_, _DirectionalLight_)

    // Return void.
    return void 0
  }

  /*
   * Remove lights from scene.
   * and also create light store.
   */
  removeLights() {
    // Loop over each light and remove light by parent.
    this.lights.forEach(__light => __light.parent.remove(__light))

    // Set light store to []
    this.lights.length = 0
  }

  /*
   * Get cubeMap texture for given
   * environment.
   */
  getCubeMapTexture(environment) {
    // Const assignment.
    const { path } = environment

    // Resolve void 0 if path is empty.
    if (!path) return Promise.resolve({ 'envMap': void 0 })

    // Resolve by using rgbe loader.
    return new Promise((__Resolve, __Reject) => {
      // Load texture of given env.
      new RGBELoader()
        .setDataType(UnsignedByteType)
        .load(path, __texture => {
          // Const assignment.
          const _envMap = this.pmremGenerator.fromEquirectangular(__texture).texture

          // Dispose old generated texture.
          this.pmremGenerator.dispose()

          // Resolve envMap.
          __Resolve({ 'envMap': _envMap })
        }, void 0, __Reject)
    })
  }


  /*
   * Clear everything or basically dispose
   * scene environment etc.
   */
  clear() {
    /*
     * If not content is available than
     * return as is.
     */
    if (!this.content) return void 0

    // Remove given content from scene.
    this.scene.remove(this.content)

    // Dispose geometry
    this.content.traverse(__node => {
      /*
       * If given node is not mesh than
       *  return as is.
       */
      if (!__node.isMesh) return void 0

      // Dispose all geometry.
      __node.geometry.dispose()

      // Return void.
      return void 0
    })

    // Dispose textures
    Viewer.traverseMaterials(this.content, __material => {
      /*
       * Loop over all maps so that
       * material can be disposed.
       */
      _maps.forEach(__map => {
        // Dispose all materials.
        if (__material[__map]) __material[__map].dispose()
      })
    })

    // Return void.
    return void 0
  }
}


/*
 * EXPORTS
 */
export default Viewer
