/*
 * IMPORTS
 */
import styled, { createGlobalStyle } from 'styled-components' // Npm: styled-components library.
import themeGet from '@styled-system/theme-get' // Npm: styled-system themeGet library.


/*
 * PACKAGES
 */
import Box from 'reusecore/src/elements/Box'


/*
 * GLOBALS
 */
const GlobalStyle = createGlobalStyle`
  * {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }
  *:focus {
    outline: none;
  }
  ::selection {
    background: #333333;
    color: #ffffff;
  }
  ::-webkit-scrollbar{
    width: 0;
    background: transparent;
  }
  ::-webkit-scrollbar-thumb {
    background: transparent;
  }
  html {
    box-sizing: border-box;
    cursor:url("cursor.svg",default)
  }
  html,
  html a,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  a,
  p,
  li,
  dl,
  th,
  dt,
  input,
  textarea,
  span,
  div {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.004);
    font-family: Poppins, serif;
  }
  body {
    margin: 0;
    padding: 0;
    -webkit-tap-highlight-color: transparent;
    letter-spacing: 0.5px;
    font-family: 'Poppins', Helvetica, Arial, sans-serif;
    transition: color 0.3s, background-color 0.5s;
  }
  ul {
    margin: 0;
    padding: 0;
  }
  li {
    list-style-type: none;
  }
  a {
    text-decoration: none;

    &:hover {
      text-decoration: none;
    }
  }
  @keyframes blinkHide {
    0% {
      opacity: 0;
    }
    10% {
      opacity: 1;
    }
  }
  @keyframes blinkShow {
    0% {
      opacity: 1;
    }
    10% {
      opacity: 0;
    }
  }
  @keyframes rotateIt {
    to {
      transform: rotate(-360deg);
    }
  }
  @keyframes ScaleInUp {
    from {
      opacity: 0;
      transform: translateY(30px) scale(0.97);
    }
    to {
      opacity: 1;
      transform: translateY(0) scale(1);
    }
  }


  @keyframes morph {
    0%,100%{
      border-radius: 42% 58% 70% 30% / 45% 45% 55% 55%;
      transform: translate3d(0,0,0) rotateZ(0.01deg);
    }
    34%{
      border-radius: 70% 30% 46% 54% / 30% 29% 71% 70%;
      transform:  translate3d(0,5px,0) rotateZ(0.01deg);
    }
    50%{
      opacity: .89;
      transform: translate3d(0,0,0) rotateZ(0.01deg);
    }
    67%{
      border-radius: 100% 60% 60% 100% / 100% 100% 60% 60% ;
      transform: translate3d(0,-3px,0) rotateZ(0.01deg);
    }
  }
  @keyframes morphWithoutOpacity {
    0%,100%{
      border-radius: 42% 58% 70% 30% / 45% 45% 55% 55%;
      transform: translate3d(0,0,0) rotateZ(0.01deg);
    }
    34%{
      border-radius: 70% 30% 46% 54% / 30% 29% 71% 70%;
      transform:  translate3d(0,5px,0) rotateZ(0.01deg);
    }
    50%{
      transform: translate3d(0,0,0) rotateZ(0.01deg);
    }
    67%{
      border-radius: 100% 60% 60% 100% / 100% 100% 60% 60% ;
      transform: translate3d(0,-3px,0) rotateZ(0.01deg);
    }
  }
  @keyframes fadeIn {
    100% {
      transform: scale(1.03);
      opacity: 0;
    }
  }
  .button {
    width: max-content;
    margin-top: 30px !important;
    margin-bottom: 30px !important;
    margin-left: 0;
    margin-right: 0;
    font-size: 15px;
    display: flex;
    border-radius: 9px;
    border-color: transparent;
    background: ${themeGet('colors.primary')};
    padding: 15px 20px;
    text-transform: capitalize;
    transition: box-shadow 0.3s ease-in-out, background 0.3s ease-in-out;
    box-shadow: 0 20px 35px ${themeGet('colors.primaryShadow')};
    pointer-events: auto;
    cursor: pointer;
    font-family: inherit;
    position: relative;
    &::before,
    &::after {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    & * {
      font-weight: 600;
      color: ${themeGet('colors.white')};
    }
    &:hover {
      background: transparent !important;
      box-shadow: none;
    }
    &.white {
      background: ${themeGet('colors.white')};
      box-shadow: 0 20px 35px ${themeGet('colors.blackShadow')};

      & * {
        font-weight: 600;
        color: ${themeGet('colors.black')};
      }

      &:hover {
        background: ${themeGet('colors.white')};
      }
    }
    &.black {
      background: ${themeGet('colors.black')};
      box-shadow: 0 20px 35px ${themeGet('colors.blackShadow')};
      & * {
        font-weight: 600;
        color: ${themeGet('colors.white')};
      }
    }
    &.theme {
      background: ${themeGet('colors.primary')};
      box-shadow: 0 20px 35px ${themeGet('colors.primaryShadow')};
      & * {
        font-weight: 600;
        color: ${themeGet('colors.white')};
      }
      &.secondary {
        background: ${themeGet('colors.secondary')};
        box-shadow: 0 20px 35px ${themeGet('colors.primaryShadow')};
        & * {
          font-weight: 600;
          color: ${themeGet('colors.white')};
        }
      }
    }
    &.success {
      background: ${themeGet('colors.success')};
      & * {
        font-weight: 600;
        color: ${themeGet('colors.success')};
      }
    }
    &.danger {
      background: ${themeGet('colors.danger')};
      & * {
        font-weight: 600;
        color: ${themeGet('colors.white')};
      }
    }
    &.surtur {
      padding: 0;
      position: relative;
      left: -30px;
      background: none;
      clip-path: circle(40% at 50% 50%);
      &:focus-visible {
        background: ${themeGet('colors.primary')};
      }
      &:hover {
        & .circle {
          animation: rotateIt 7s linear infinite;
        }
        & .eyeLashesUp,
        & .eyeInner,
        & .eyeIris {
          animation: blinkHide 2s step-end infinite;
        }
        & .eyeLashesDown {
          animation: blinkShow 2s step-end infinite;
        }
      }
      & .circle {
        position: relative;
        display: block;
        width: 200px;
      }
      & .circle text {
        font-size: 32px;
        text-transform: uppercase;
        fill: ${themeGet('colors.black')};
      }
      & .circle .textPath {
        letter-spacing: 17px;
      }
      & .eye {
        position: absolute;
        z-index: 10;
        width: 60px;
        height: 60px;
        top: calc(50% - 30px);
        left: calc(50% - 30px);
        &Outer,
        &Inner,
        &LashesUp,
        &LashesDown {
          stroke: ${themeGet('colors.black')};
          fill: none;
          stroke-width: 1.5px;
        }
        &LashesDown {
          opacity: 0;
        }
      }
    }
  }
  .subHeading {
    text-transform: uppercase;
    justify-content: center;
    margin-right: auto;
    margin-bottom: 0;
    margin-left: 0;
    position: relative;
    text-align: left;
    font-size: 15px;
    width: min-content;
    font-weight: 600;
    box-shadow: 0 20px 35px ${themeGet('colors.blackShadow')};
    background-color: ${themeGet('colors.primary')};
    border-radius: 25px;
    padding: 2.5px 10px !important;
    color: ${themeGet('colors.white')};
    &, & * {
      color: ${themeGet('colors.white')};
    }

    &.revert {
      &::before {
        left: unset;
        right: -30px;
      }
    }

    &.white {
      background-color: ${themeGet('colors.white')};
      color: ${themeGet('colors.black')} !important;
      &::before {
        background-color: ${themeGet('colors.white')};
      }
    }

    &.theme {
      background-color: ${themeGet('colors.primary')};
      color: ${themeGet('colors.white')} !important;
      box-shadow: 0 0 30px 3px ${themeGet('colors.primaryShadow')};

      &::before {
        background-color: ${themeGet('colors.primary')};
        box-shadow: 0 0 30px 3px ${themeGet('colors.primaryShadow')};
      }
      &.secondary {
        background-color: ${themeGet('colors.secondary')};
        color: ${themeGet('colors.white')} !important;
        box-shadow: 0 0 30px 3px ${themeGet('colors.secondaryShadow')};

        &::before {
          background-color: ${themeGet('colors.secondary')};
          box-shadow: 0 0 30px 3px ${themeGet('colors.secondaryShadow')};
        }
      }
    }
    &.black {
      background-color: ${themeGet('colors.black')};
      color: ${themeGet('colors.white')};
      box-shadow: 0 0 30px 3px ${themeGet('colors.blackShadow')};

      &::before {
        background-color: ${themeGet('colors.black')};
        box-shadow: 0 0 30px 3px ${themeGet('colors.blackShadow')};
      }
    }
    
    &.danger {
      background-color: ${themeGet('colors.danger')};
      color: ${themeGet('colors.white')} !important;
      box-shadow: 0 0 30px 3px ${themeGet('colors.blackShadow')};

      &::before {
        background-color: ${themeGet('colors.danger')};
        box-shadow: 0 0 30px 3px ${themeGet('colors.blackShadow')};
      }
    }
    
    @media (max-width: 766px) {
      font-size: 14px;
    }
    @media (max-width: 556px) {
      font-size: 13px;
    }
  }
  .heading,
  .selfWritingHeading {
    font-size: 91px;
    font-weight: 900;
    letter-spacing: 0.015em;
    line-height: 1.2;
    text-align: left;
    margin-top: 30px !important;
    margin-right: 0;
    margin-bottom: 0;
    margin-left: 0;
    color: ${themeGet('colors.primary')};
    &.danger {
      color: ${themeGet('colors.danger')};
    }
    @media (max-width: 1300px) and (min-width: 766px) {
      font-size: 81px;
    }
    @media (max-width: 766px) {
      font-size: 71px;
    }
    @media (max-width: 556px) {
      font-size: 51px;
    }
  }
  .selfWritingHeading {
    font-weight: 200;
  }
  * ~ .subHeading,
  .subHeading ~ * {
    margin-top: 10px !important;
  }
  .subHeading ~ .heading {
    margin-top: 10px !important;
  }
  .description {
    font-size: 18px;
    line-height: 1.75;
    font-weight: 400;
    text-align: left;
    width: 60%;
    margin-top: 40px !important;
    margin-right: auto;
    margin-bottom: 0;
    margin-left: 0;
    color: ${themeGet('colors.black')};
    @media (max-width: 1220px) {
      width: 90%;
    }
    @media (max-width: 991px) {
      width: 100%;
    }
    @media (max-width: 556px) {
      font-size: 16px;
    }
  }
  .description ~ .button {
    margin-top: 60px !important;
  }
  .dIcon {
    position: relative;
    height: 90px;
    width: 90px;
    display: flex;
    align-items: center;
  }
  .dBanner {
    border-radius: 42% 58% 70% 30% / 45% 45% 55% 55%;
    width: calc(100vw / 1.5);
    height: calc(100vw / 1.5);
    position: absolute;
    margin: auto;
    will-change: border-radius;
    z-index: 10000;
    &:before,
    &:after{
      content: '';
      width: 100%;
      height: 100%;
      display: block;
      position: absolute;
      left: 0; top: 0;
    }
    &:after{
      line-height: 120px;
      text-indent: -21px;
    }
    &.positionCenter {
      top: calc(25% - 100px);
    }
    &.positionRight {
      right: 0;
      left: unset;
    }
    &.positionBottom {
      bottom: 0;
    }
    &.positionTop {
      top: 0;
    }
    &.positionLeft {
      left: 0;
      right: unset;
    }
    @media only screen and (max-width: 991px) {
      display: none;
    }
  }
  .svg {
    &.stroke {
      &.theme {
        stroke: ${themeGet('colors.primary')};
        &.secondary {
          stroke: ${themeGet('colors.secondary')};
        }
      }
      &.white {
        stroke: ${themeGet('colors.white')};
      }
      &.blackShadow {
        stroke: ${themeGet('colors.blackShadow')};
      }
      &.darkWhite {
        stroke: ${themeGet('colors.darkWhite')};
      }

      &.lightDarkWhite {
        stroke: ${themeGet('colors.lightDarkWhite')};
      }
    }

    &.fill {
      &.theme {
        fill: ${themeGet('colors.primary')};

        &.secondary {
          fill: ${themeGet('colors.secondary')};
        }
      }
      &.white {
        fill: ${themeGet('colors.white')};
      }
      &.blackShadow {
        fill: ${themeGet('colors.blackShadow')};
      }
      &.darkWhite {
        fill: ${themeGet('colors.darkWhite')};
      }
      &.lightDarkWhite {
        fill: ${themeGet('colors.lightDarkWhite')};
      }
    }
  }
  .font.whiteSpacePre {
    white-space: pre;
  }
  .font.black {
    span&,
    a&,
    p&,
    h1&,
    h2&,
    h3&,
    h4&,
    h5&,
    h6&,
    span,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    &.selfWritingHeading {
      color: ${themeGet('colors.black')};
    }

    &.subHeading {
      &::before {
        background-color: ${themeGet('colors.black')};
      }
    }
  }
  .font.white {
    span&,
    a&,
    p&,
    h1&,
    h2&,
    h3&,
    h4&,
    h5&,
    h6&,
    span,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    &.selfWritingHeading {
      color: ${themeGet('colors.white')};
    }

    &.subHeading {
      &::before {
        background-color: ${themeGet('colors.white')};
      }
    }
  }
  .font.danger {
    span&,
    a&,
    p&,
    h1&,
    h2&,
    h3&,
    h4&,
    h5&,
    h6&,
    span,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    &.selfWritingHeading {
      color: ${themeGet('colors.danger')};
    }

    &.subHeading {
      &::before {
        background-color: ${themeGet('colors.danger')};
      }
    }
  }
  .font.theme {
    span&,
    a&,
    p&,
    h1&,
    h2&,
    h3&,
    h4&,
    h5&,
    h6&,
    span,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    &.selfWritingHeading {
      color: ${themeGet('colors.primary')};
    }

    &.subHeading {
      &::before {
        background-color: ${themeGet('colors.primary')};
      }
    }
  }
  .font.theme.secondary {
    span&,
    a&,
    p&,
    h1&,
    h2&,
    h3&,
    h4&,
    h5&,
    h6&,
    span,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    &.selfWritingHeading {
      color: ${themeGet('colors.secondary')};
    }
    &.subHeading {
      &::before {
        background-color: ${themeGet('colors.secondary')};
      }
    }
  }
  .width.maxContent {
    width: max-content;
  }
  .width.minContent {
    width: min-content;
  }
  div.scrollbar-track.scrollbar-track-x.show,
  div.scrollbar-track.scrollbar-track-y.show {
    display: none !important;
  }
`
const AppWrapper = styled(Box)`
  & #VerticalScroll {
    height: 100vh;
    width: 100vw;
    overflow: hidden;
    transition: background-color 0.3s ease-in-out, color 0.5s ease-out;
    & .scrollbar-track,
    & .scrollbar-thumb {
      display: none !important;
    }
  }
  & .particle {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
  }
`


/*
 * EXPORTS
 */
export { GlobalStyle, AppWrapper }
