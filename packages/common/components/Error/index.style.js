/*
 * IMPORTS
 */
import styled from 'styled-components'// Npm: Styled components library
import themeGet from '@styled-system/theme-get' // Npm: styled-system themeGet library.


/*
 * PACKAGES
 */
import Box from 'reusecore/src/elements/Box'


/*
 * EXPORTS
 */
const ErrorWrapper = styled(Box)`
  padding: 60px 15px;
  min-height: 100vh;
  display: flex;
  overflow: hidden;
  align-items: center;
`
const ErrorContent = styled(Box)`
  margin: 0 auto;
  text-align: center;
  & .logo {
    width: 100%;
    height: calc(100vh / 3);
    margin-bottom: 60px;
    margin-left: auto;
    margin-right: auto;
    position: relative;
  }
  .heading,
  .description {
    text-align: center;
  }
  .description {
    width: 55%;
    font-weight: 600;
    margin-left: auto;
    margin-right: auto;
    @media (max-width: 991px) {
      width: 60%;
    }
    @media (max-width: 731px) {
      width: 90%;
    }
    @media (max-width: 667px) {
      width: 80%;
    }
  }
`


/*
 * EXPORTS
 */
export {
  ErrorWrapper,
  ErrorContent
}
