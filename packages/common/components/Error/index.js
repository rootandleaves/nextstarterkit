/*
 * IMPORTS
 */
import React from 'react'// Npm: React.js Library.
import PropTypes from 'prop-types' // Npm: react prop validation library.


/*
 * PACKAGES
 */
import Text from 'reusecore/src/elements/Text'
import Image from 'reusecore/src/elements/Image'
import Heading from 'reusecore/src/elements/Heading'


/*
 * STYLES
 */
import {
  ErrorContent,
  ErrorWrapper
} from './index.style'


/*
 * OBJECTS
 */
const Index = ({ statusCode }) => {
  // Const assignment.
  const _errorDescription = {
    '404': "Looks like the page you're trying to visit doesn't exist. Please check the URL."
  }

  // Return component.
  return (
    <ErrorWrapper>
      <ErrorContent>
        <div className='logo'>
          <Image src='/static/image/404.svg' layout='fill'/>
        </div>
        <Heading content={statusCode} className='heading'/>
        <Text
          className='description'
          content={_errorDescription[statusCode] ?? 'Oh No! Something went wrong. Please go back or try again later on same thing. Sorry for inconvenience.'}
        />
      </ErrorContent>
    </ErrorWrapper>
  )
}


/*
 * PROPTYPES
 */
Index.propTypes = {
  'statusCode': PropTypes.oneOfType([PropTypes.number, PropTypes.string])
}
Index.defaultProps = {
  'statusCode': '404'
}


/*
 * EXPORTS
 */
export default Index
