/*
 * IMPORTS
 */
import Colors from '../common'


/*
 * GLOBALS
 */
const colors = {
  ...Colors,
  'primary': '#2948ff',
  'primaryDark': '#1c2883',
  'primaryShadow': 'rgba(41, 72, 255, 0.13)',
  'secondary': '#396afc',
  'secondaryShadow': 'rgba(57, 106, 252, 0.13)'
}


/*
 * EXPORTS
 */
export default colors
