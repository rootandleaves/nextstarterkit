/*
 * IMPORTS
 */
import { variant } from 'styled-system' // Npm: styled-system library.

/*
 * OBJECTS
 */
const buttonStyle = variant({ key: 'buttonStyles' })
const colorStyle = variant({ key: 'colorStyles', prop: 'colors' })
const sizeStyle = variant({ key: 'sizeStyles', prop: 'size' })

/*
 * EXPORTS
 */
export const cards = variant({ key: 'cards' })
export { buttonStyle, colorStyle, sizeStyle }
