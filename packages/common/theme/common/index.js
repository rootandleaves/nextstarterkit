/*
 * GLOBALS
 */
const index = {
  'transparent': 'transparent',
  'success': '#4caf50',
  'danger': '#ff005c',
  'blue': '#464488',
  'black': '#0f0c29',
  'blackShadow': 'rgba(0, 0, 0, 0.06)',
  'blackShadowHover': 'rgba(16, 66, 97, 0.1)',
  'facebook': '#1778F2',
  'facebookShadow': 'rgba(23, 120, 242, 0.06)',
  'facebookHover': '#1461c2',
  'lightBlue': '#00acee',
  'darkBlue': '#464488',
  'darkerWhite': '#acacac',
  'purple': '#9D82FD',
  'semiLightBlack': '#292929',
  'darkestWhite': '#dbdbdb',
  'lightestBlack': '#323232',
  'lightBlack': '#202020',
  'darkBlack': '#101010',
  'lightDarkWhite': '#f8f8f8',
  'darkWhite': '#e6e6e6',
  'yellow': '#FFEA00',
  'white': '#ffffff',
  'whatsApp': '#48C458',
  'whatsAppHover': '#3ca149',
  'whatsAppShadow': 'rgba(72, 196, 88, 0.03)',
  'teams': '#464EB8',
  'teamsHover': '#3c429b',
  'teamsShadow': 'rgba(70, 78, 184, 0.03)',
  'instagram': '#8A49A1',
  'instagramHover': '#C1558B',
  'instagramShadow': 'rgba(193, 85, 139, 0.03)'
}


/*
 * EXPORTS
 */
export default index
