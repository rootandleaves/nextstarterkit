/*
 * PACKAGES
 */
import * as theme from '~packages/common/theme'


/*
 * OBJECTS
 */
const _ThemeReducer = (__prevState = theme.DEFAULT, __action) => {
  /*
   * Switch case for handling actions on
   * theme store.
   */
  switch (__action.type) {
  case 'THEME_SET':
    // Return combined state.
    return {
      ...__prevState,
      ...theme[__action.Theme]
    }
  default:
    // Return old state.
    return __prevState
  }
}


/*
 * EXPORTS
 */
export default _ThemeReducer
