# How to Configure

1. `yarn` in the main directory.
2. `yarn dev` on the main directory

## Stack We Used

1. Lerna (A tool for managing JavaScript projects with multiple packages. https://lernajs.io)
2. Yarn Workspace.
3. React Js and Next Js.
4. Styled System and Styled Components

## Folder Structure

The whole work is in the `command/source` folder. After entering the folder, You will find some other folders.

1. components
2. containers
3. contexts
4. data
5. pages

and also some configuration like .babelrc, next.config.js, etc. Now, we will discuss every folder and their task.

# next.config.js

We have used some plugins for better performance and optimization.

1. next-optimized-images
2. next-fonts
3. next-css

#components

If you are familiar with react or create react app architecture, then we are familiar with components. Components are reusable codes that you will use throughout your project. Here in the components folder, we wrote some custom components which are used on our website. We have done some basic style with the styled-components (https://www.styled-components.com/).

Under the reuse core folder, you will find some basic components like Text, Heading, Image, Input etc. We have written these components to make the developer's life easy. By using these basic components, you can write custom components according to our needs. In the packages/common -> components folder, we have done the same things. We have written some custom components for our WebSites by using these reuse core components.

#containers

Under the containers folder, we have written all of the parts of our code by part. Suppose, we have 2 WebSites.

1. rnl.
2. transportation.

In each folder, we have all the sections of that website part by part like Navbar, BannerSection, FeatureSection, TestimonialSection, Footer etc. This structure is done for a perfect understanding of each section. We have also a custom index.style.js file like (transportation.index.style.js, rnl.index.style.js, etc) for common styles of that specific website.

We have used two libraries to style our Components.

1. styled-components. https://www.styled-components.com/docs
2. styled-system. See their doc from here https://github.com/jxnblk/styled-system

#pages
As we have used next.js, we have a script for your package.json like this:

{
"scripts": {
"clean": "lerna clean --yes && rimraf node_modules",
"start": "lerna run --parallel dev",
"dev": "next",
"now-build": "NODE_ENV=production next build",
"website-build": "rimraf dist && next build",
"website-start": "next start"...
}
}

After that, the file-system is the main API. Every .js file becomes a route that gets automatically processed and rendered.

Please have a look at this link https://nextjs.org/docs/ for a quick look. You will understand the basic things so quickly.

As we have 2 websites named rnl and transportation so we create 2 .js files named rnl.js, transportation.js.

In these main js files, we have imported all the codes written in the containers folder step by step.
Every .js file becomes a route that gets automatically processed and rendered in next.js,so you will find your,
rnl website at http://localhost:3000/
transportation website at http://localhost:3000/transportation

Here, in the index.js file, we have assigned the rootandleaves website.

#static

In the static folder, you will find all of the common css files in the css folder, all of the fonts used in the website in the fonts folder and all of the images under the image folder.

#theme
In the theme folder, you will also find some folders.

1. rnl.
2. transportation.

In each folder, you will find the three .js files
colors.js: in this file, you can keep all of the custom colors for your specific website.
customeVariant.js: For writing custom variants
index.js: all of the style props.

We have used the styled system for this folder structure (https://github.com/jxnblk/styled-system ). You can also follow this article for clearing your concept https://varun.ca/styled-system/.

#data

In the data folder, you will find the specific data that are used on that specific website.

## Icons

We use a custom flaticon. If you want to see our icon list then you need to go to the icons page. After running the `yarn dev` command then go to your browser and write `http://localhost:3000/icons` and hit enter.
